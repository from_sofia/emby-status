# Emby status

Simple bash script providing some basic info for the mountpoints /mnt/movies and /mnt/series used by the [emby-server](https://emby.media). One should modify lines 8-13 to reflect their physical paths.
The script will show:
 - available space for the mounted drive housing your emby data
 - count number of directories and their size for both your series and movies paths
 - indicate service is running and provide you it's PID
 - provide you the URL needed to access emby /assuming one is running emby on the default port/
