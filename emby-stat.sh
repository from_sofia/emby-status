#!/bin/bash

sync
echo " " 
echo "************************"
echo "Media has been mounted:"
echo "************************"
df -hT /mnt

size_ser="$(du -sh /mnt/series | awk '{print $1}')"
size_mov="$(du -sh /mnt/movies | awk '{print $1}')"
count_mov="$(ls -l /mnt/movies/ | wc -l)"
count_ser="$(ls -l /mnt/series/ | wc -l)"

echo " "
echo "**********"
echo "Contents:"
echo "**********"
echo "$count_mov movies in $size_mov and $count_ser tv shows in $size_ser"

echo " "
echo "*********************"
echo "Emby is now running:"
echo "*********************"
systemctl status emby-server.service | grep -i active: | cut -c 4-
systemctl status emby-server.service | grep -i "main pid:" | cut -c 2-

echo " "
echo "************************************************"
echo "The UI Accessible on http://$(hostname):9153"
echo "************************************************"

